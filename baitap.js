/**
 * Bài 1
 * 
 * b1: input
 * khai báo biến ngày công , tiền công;
 * 
 * b2: xử lí
 * lấy ngày công x tiền công
 * 
 * b3: output
 * trả kết quả bằng console.log
 */
var workDay = 24
var wage = 100000

var payWage = workDay * wage

console.log(payWage)

/**
 * Bài 2
 * 
 * b1: input
 * khai báo 5 biến và gán giá trị cho 5 biến
 * 
 * b2: xử lí
 * Cộng các biến lại với nhau chia cho 5
 * 
 * b3: output
 * trả kết quả bằng console.log
 */

var a = 10
var b = 20
var c = 30
var d = 40
var e = 50

var result = a + b + c + d + e / 5

console.log(result)


/**
 * Bài 3
 * 
 * b1: input
 * Tạo biến cho tiền tệ USD và tiền tệ VND
 * 
 * b2: xử lí
 *  Lấy Biến USD x Biến VND
 * 
 * b3: output
 * trả kết quả bằng console.log
 */

var VND = 23500
var USD = 2

var usdConvertVnd = USD * VND

console.log(usdConvertVnd, "VND")

/**
 * Bài 4
 * 
 * b1: input
 * tạo 2 biến Dài, rộng và gán giá trị cho 2 biến
 * 
 * b2: xử lí
 * Tính diện tích hình chữ nhật : S = Long*width
 * Tính chu vi hình chữ nhật : P = (Long*width)*2
 * 
 * b3: output
 * trả kết quả bằng console.log
 */

var Long = 20
var Width = 30

var clacRectangleS = Long * Width
var clacRectangleP = (Long + Width) * 2

console.log("clacRectangleS: ", clacRectangleS, "clacRectangleP: ", clacRectangleP)

/**
 * Bài 5
 * 
 * b1: input
 * Khai báo biến và gán giá trị 2 chữ số 
 * 
 * b2: xử lí
 * Đặt biến lấy số đơn vị Số / 10
 * Đặt biến lấy số hàng chục Số % 10
 * 
 * 
 * b3: output
 * cộng 2 biến lại với nhau và dùng Math.floor() để làm tròn xuống cho biến lấy Đơn vị 
 * trả kết quả bằng console.log
 */

var number = 66

var SumNumberIntDozents = number / 10

var SumNumberIntUnits = number % 10


console.log(SumNumberIntUnits + Math.floor(SumNumberIntDozents))